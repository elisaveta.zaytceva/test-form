import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InputNameOneLetterTest extends BaseTest{

    @Test
    public void inputNameOneLetterTest(){
        dataInput.dataInput("В","test@mail.ru","89236402947","Значимость этих проблем настолько очевидна, что консультация с широким активом играет важную роль в формировании новых предложений. Товарищи! консультация с широким активом позволяет выполнять важные задания по разработке систем массового участия. Таким образом реализация намеченных плановых заданий позволяет оценить значение новых предложений.");
        Assert.assertFalse(driver.findElement(By.xpath("//h1")).isDisplayed(),"Можно отправить имя, состоящее из 1й буквы");
    }
}
