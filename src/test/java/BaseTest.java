import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


public class BaseTest {
    protected WebDriver driver = CreateDriver.createDriver();
    protected LifeHelper lifeHelper = new LifeHelper(driver);
    protected DataInput dataInput = new DataInput(driver);

    @BeforeClass
    public void open() throws InterruptedException {
        driver.get("http://test-form.sibirix.ru/");
        Thread.sleep(2000);
    }

    @AfterClass
    public void close(){
        driver.quit();
    }

}
