import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LifeHelper {
    protected WebDriver driver;
    public LifeHelper(WebDriver driver){
        this.driver = driver;
    }

    public void open(String url){
        driver.get(url);
    }

    public WebElement waitElementIsVisible(WebElement element){
        new WebDriverWait(driver, 4)
                .until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public WebElement waitElementIsInVisible(WebElement element){
        new WebDriverWait(driver, 4)
                .until(ExpectedConditions.invisibilityOf(element));
        return element;
    }
}

