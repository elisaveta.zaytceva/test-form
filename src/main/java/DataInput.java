import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DataInput extends LifeHelper {

    public DataInput(WebDriver driver) {
        super(driver);
    }

    public DataInput dataInput (String name, String email, String phoneNumber, String aboutProject) {
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(name);
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys(email);
        driver.findElement(By.xpath("//input[@name='phone']")).sendKeys(phoneNumber);
        driver.findElement(By.xpath("//textarea[@name='message']")).sendKeys(aboutProject);

        driver.findElement(By.xpath("//input[@type='submit']")).click();


        return this;

    }
}
